#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb  7 11:37:01 2021

@author: Marek Hanus
"""
# import system modules
import sys
sys.settrace
import os
import subprocess
import time
import datetime

# import tkinter and matplotlib modules
from tkinter import *
from tkinter import filedialog
import tkinter
import tkinter.messagebox
import tkinter.ttk as ttk
from tkscrolledframe import ScrolledFrame


import reportlab


import matplotlib.animation as animation
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
from matplotlib.backends.backend_pdf import PdfPages

import csv
import scipy
import numpy as np

from PIL import ImageTk, Image

import configparser
import threading

root = tkinter.Tk()

class Chart:
    def init(self):
        self.fig = plt.figure()
        self.ax1 = plt.subplot(111)
        self.resetVar()
        self.drawGui()
        
        
        self.t = threading.Thread(target=self.start_c)
        self.t.start()
        
    def resetVar(self):
        self.mywidth = 1024
        
        self.myheight = 600
        
        self.startBool = False
        self.animation = True
        
        self.pan = False
        self.cycle = 0
        self.rownum = 0
        self.grafX = [0] * 100
        self.style = ["-"] * 100
        self.grafY = [0] * 100
        self.col = [0] * 100
        self.vis = [0] * 100
        self.checkVis = [0] * 100
        self.C1 = [0] * 100
        self.buttonStyle = [0] * 100
        self.labelAnalyze = [0] * 100
        self.buttonAnalyze = [0] * 100
        self.styleName = ["Line"] * 100
        self.dataShow = 0
        self.default = 0
        self.globalstop = 0
        self.zoomed = False
        self.activation = False
        self.preview = False
        self.integral = []
        self.maxX = []
        self.secondStart = False
        
        for i in range(0, 20):
            self.vis[i] = BooleanVar()
        
        self.x_max = 0
        self.x_min = 0
        self.y_max = [-float("inf")] * 100
        self.y_min = [float("inf")] * 100
        self.timestamp = "0"
        self.csvfile = "ChromaDLo_data.csv"
        self.pixelVirtual = tkinter.PhotoImage(width=1, height=1)
        self.rowPopis = 0
        self.mytime = 0
        
    def drawGui(self):
        # set window properties
        root.geometry("{0}x{1}+0+0".format(self.mywidth, self.myheight)) # set window to fullscreen
        root.wm_title("ChromaDLo") # title
        
        self.canvas2 = tkinter.Canvas(root)
    
        self.scrollbar = tkinter.Scrollbar(root)
        self.scrollbar.pack(side=tkinter.RIGHT, fill='y')
        self.canvas2.pack(side = "right")
        
        self.scrollbar.config(orient=tkinter.VERTICAL, command=self.canvas2.yview)
        self.canvas2.configure(yscrollcommand = self.scrollbar.set)
        self.canvas2.configure(width = int((self.mywidth / 100) * 21), height = self.myheight*1.5)
        
        #scrollbar.pack(fill=tkinter.Y, side=tkinter.RIGHT, expand=tkinter.FALSE) 
        
        # update scrollregion after starting 'mainloop'
        # when all widgets are in canvas
        self.canvas2.bind('<Configure>', self.on_configure)
        
        # --- put frame in canvas ---
        
        self.frame = tkinter.Frame(self.canvas2)
        self.canvas2.create_window((0,0), window=self.frame, anchor='nw')
        
        self.histogramFrame = Frame(self.frame, width = (self.mywidth / 100) * 50, height = self.myheight*1.5)
        self.histogramFrame.pack()
        
        # update scrollregion after starting 'mainloop'
        # when all widgets are in canvas
        self.canvas2.bind('<Configure>', self.on_configure)
        
        # --- put frame in canvas ---
        
        ############### MATPLOTLIB IMPLEMENTATION ###############
        self.canvas = FigureCanvasTkAgg(self.fig, master=root)  
        
        self.canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)
        
        self.toolbar = NavigationToolbar2Tk(self.canvas, root)
        
        self.toolbar.update()
        self.toolbar.place(x = (self.mywidth / 100) * 55.2, y = (self.myheight / 100) * 89)
        
        self.fig.canvas.toolbar.zoom()
        
        toolHider = tkinter.Frame(root, width = 230.5, height = 40, bg = "white", bd = 0)
        toolHider.place(x = (self.mywidth / 100) * 55, y = (self.myheight/100) * 88.8) 
        
        ############### MENUBAR ###############
        self.menubar = tkinter.Menu(root)
        
        self.filemenu = tkinter.Menu(self.menubar, tearoff=0)
        self.optionsmenu = tkinter.Menu(self.menubar, tearoff=0)
        self.aboutmenu = tkinter.Menu(self.menubar, tearoff=0)
        
        
        
        # add menu
        
        self.menubar.add_cascade(label="File", menu=self.filemenu)
        self.menubar.add_cascade(label="Settings", menu=self.optionsmenu)
        self.menubar.add_cascade(label="About", menu=self.aboutmenu)
        
        # add menu commands
        self.filemenu.add_command(label="Open", command=self.csvUpload, accelerator="Ctrl+O")        
        self.optionsmenu.add_command(label="Metodika", command=self.Metodika, accelerator="Ctrl+M")
        self.aboutmenu.add_command(label="Exit", command=root.quit, accelerator="Ctrl+Q")
        #self.aboutmenu.add_command(label="Help", command=self.pomoc, accelerator="Ctrl+H")
        
        root.config(menu=self.menubar)
        
        # binding keyboard shortcuts
        root.bind('<Control-q>', root.quit)
        root.bind('<Control-o>', self.ctrlO)
        root.bind('<Control-d>', self.ctrlD)
        root.bind('<Control-x>', self.ctrlX)
        
        root.bind("<Control-C>", self.ctrlC)
       
        root.bind('<Shift_L>', self.shift)
        root.bind('<KeyRelease-Shift_L>', self.shift_release)
        
        root.bind("<Control-m>", self.ctrlm) 
        
        plt.subplots_adjust(left=0.062, right=0.9, top=0.96, bottom=0.15)
        
        self.fig.canvas.mpl_connect('motion_notify_event', self.onmotion)
        self.fig.canvas.mpl_connect('pick_event', self.onpick)
        self.fig.canvas.mpl_connect('button_release_event', self.onrelease) 
        
        # set start button
        self.const = int((root.winfo_screenwidth() / 9))
        self.img = Image.open("malyPreview.png")
        self.width2, self.height2 = self.img.size
        self.img = self.img.resize((self.const, int((self.const / self.width2) * self.height2)), Image.ANTIALIAS)
        self.photo = ImageTk.PhotoImage(self.img)
        print("cau")
        self.startbutton = tkinter.Button(self.histogramFrame, image=self.photo, command = self.start)         
        self.startbutton.image = self.photo
        self.startbutton.place(x=95, y = 120)
        
        self.readINI()
        self.config["STATUS"]["state"] = "stop"
        self.config["STATUS"]["preview"] = "Disable"
        self.writeINI()
        
        self.xminEntry = 0
        self.xmaxEntry = 0
        self.y1minEntry = 0
        self.y1maxEntry = 0
        
        self.VzorekLabel = tkinter.Label(self.histogramFrame, text="Název vzorku:")
        self.VzorekLabel.place(x=10,y=0)
        self.VzorekEntry = tkinter.Entry(self.histogramFrame, width = 10)
        self.VzorekEntry.place(x = 105, y = 0)
        
        self.PopisLabel = tkinter.Label(self.histogramFrame, text="Popis měření:")
        self.PopisLabel.place(x=10,y=17)
        self.PopisEntry = tkinter.Text(self.histogramFrame, width = 25, height = 5)
        self.PopisEntry.place(x=7,y=35)
        
        
        config = self.readcsv("NazevVzorku.csv")
        self.rowPopis = self.rownum-1
        print(config)
        self.VzorekEntry.insert(0,config[0][0])
        print(self.rownum, self.rowPopis)
        
        
        for i in range(self.rowPopis):
            self.PopisEntry.insert(INSERT,config[i+1][0] + "\n")   

    def on_configure(self, event):
        self.canvas2.configure(scrollregion=self.canvas2.bbox('all'))
    
    def addCsvFile(self):
            cas = time.time()
            self.mytime = datetime.datetime.utcfromtimestamp(cas).strftime("_%d-%m-%Y_%H-%M-%S")
            
            self.readINI()
            self.timestamp = self.VzorekEntry.get() + self.mytime
            self.config["METODIKA"]["time"] = self.timestamp
            self.writeINI()
    
    def start_c(self):
        subprocess.call("./ADC1262_test", shell=True)
    
    def start(self):
        if(self.secondStart == True):
            os.execl(sys.executable, sys.executable, *sys.argv)
        
        elif(self.preview == False and self.startBool == False):
            #********************************* Preview mod ********************************
            print("preview >............................................................................................")
            
            self.img = Image.open("malyStart.png")
            self.img = self.img.resize((self.const, int((self.const / self.width2) * self.height2)), Image.ANTIALIAS)
            self.photo = ImageTk.PhotoImage(self.img)
            self.startbutton.configure(image=self.photo)
            
            self.preview = True
            self.animation = True
            self.readINI()
            self.config["STATUS"]["preview"] = "Enable"
            self.writeINI()
            
            self.ax1.clear()
            
            time.sleep(2) 
            if(self.animation == True):
                self.extension = "csv"
                self.file_path = self.csvfile
                self.liveChart()
                
            
            
            
        elif(self.startBool == False):
            #****************************** Start mod **************************************
            self.img = Image.open("malyStop.png")
            self.img = self.img.resize((self.const, int((self.const / self.width2) * self.height2)), Image.ANTIALIAS)
            self.photo = ImageTk.PhotoImage(self.img)
            
            self.startbutton.configure(image=self.photo)
            
            self.startbutton.image = self.photo
            self.startBool = True
            
            self.addCsvFile()
            config = []
            
            file = open("NazevVzorku.csv", "w")
            
            
            file.write(self.VzorekEntry.get() + " \n")
            
            for i in range(15):
                print(len(self.PopisEntry.get(str(i+1.0), str(i+2.0))))
                #config.append(self.PopisEntry.get(str(i+1.0), str(i+2.0)))
                #print(self.PopisEntry.get(str(i+1.0), str(i+2.0)))
                if(len(self.PopisEntry.get(str(i+1.0), str(i+2.0))) > 2):
                    file.write(self.PopisEntry.get(str(i+1.0), str(i+2.0)))
                    print(self.PopisEntry.get(str(i+1.0), str(i+2.0)))
                    
            
            #print(config)
            
            """
            for radky in range(len(config)):
                for sloupce in range(len(config[radky])):
                    print(str(config[radky][sloupce]))
                    #if(self.config[radky][sloupce] != ""):
                    file.write(str(config[radky][sloupce])) 
            
            
            
            file.write(self.VzorekEntry.get()+" \n") #,self.PopisEntry.get("1.0", END)
            file.write(self.PopisEntry.get("1.0", "2.0"))
            file.write(self.PopisEntry.get("2.0", "3.0"))
            file.write(self.PopisEntry.get("3.0", "4.0"))
            file.write(self.PopisEntry.get("4.0", "5.0"))
            file.write(self.PopisEntry.get("5.0", "6.0"))
            """
            file.close()
            
            
            self.grafX = [0] * (self.axes-1)
            self.grafY = [0] * (self.axes-1)
            self.cycle = 0
            self.rownum = 0
            self.preview = False
            
            self.readINI()
            self.config["STATUS"]["preview"] = "Disable"
            self.config["STATUS"]["state"] = "start"
            self.writeINI()
            self.ax1.clear()

            time.sleep(2)

            if(self.animation == True):
                self.extension = "csv"
                self.file_path = self.csvfile
                self.liveChart()  
        
        else:
            #************************************ Stop mod **********************************
            self.img = Image.open("reset.png")
            self.img = self.img.resize((self.const, int((self.const / self.width2) * self.height2)), Image.ANTIALIAS)
            self.photo = ImageTk.PhotoImage(self.img)
            
            self.startbutton.configure(image=self.photo)
            
            self.startbutton.image = self.photo
            self.startBool = False
            
            self.readINI()
            self.config["STATUS"]["state"] = "stop"
            self.globalstop = 1
            self.writeINI()
            self.animation = False
            self.secondStart = True
            
            self.t.join()
            
            for i in range(self.endpoint):
                self.labelAnalyze[i].destroy()
                self.buttonAnalyze[i] = tkinter.Button(self.histogramFrame, text="Analyze", image=self.pixelVirtual, width=int((self.mywidth / 100) * 4), height=int((self.myheight / 100) * 1.17), compound=tkinter.CENTER, command = lambda ind=i: self.analyze(ind))
                self.buttonAnalyze[i].place(x=145, y=220 + (20*i))
                self.title.configure(text="Series      Axis  Style   Analyze  ")
        
    
    
        
    def drawChartWidgets(self):
        self.lab = [0] * (self.axes-1)
        self.endpoint = self.axes - 1
        
        self.title = Label(self.histogramFrame, text="Series       Axis  Style     Value  ", foreground='black', background='gray')
        self.title.place(x=8 ,y=200)
        
        self.buttonDefault = tkinter.Button(self.histogramFrame, text="Default view", image=self.pixelVirtual, width=int((self.mywidth / 100) * 5), height=int((self.myheight / 100) * 2), compound=tkinter.CENTER, command=self.defaultView,font=("TkDefaultFont", 9))
        self.buttonDefault.place(x=10, y=150)
        
        self.buttonAdjust = tkinter.Button(self.histogramFrame, text="Adjust view", image=self.pixelVirtual, width=(self.mywidth / 100) * 5, height=(self.myheight / 100) * 2, compound=tkinter.CENTER, command=self.adjustView, font=("TkDefaultFont", 9))
        self.buttonAdjust.place(x=10, y=120)


        self.labelCanvas = Canvas(self.histogramFrame, width = 73, height = 20 * self.endpoint, bg="white")
        self.labelCanvas.place(x = 10, y = 220)
        
        self.readINI()
        self.readMetod(self.config["METODIKA"]["file"])
        
        
        
        for i in range(0, self.endpoint):
            Nazev = self.configFileData[i][1]
            self.lab[i] = Label(self.histogramFrame, text=Nazev, foreground='#%02x%02x%02x' % self.col[i], background='white') 
            self.lab[i].place(x=8,y= 220 + (20*i))
         
            self.lab[i].bind("<Button-1>", lambda event, ind = i: self.colorPicker(ind))
           
            self.C1[i] = tkinter.Checkbutton(self.histogramFrame, variable=self.vis[i], width=1, height=1, background='white', onvalue=False, offvalue=True, command=self.setVisibility)
            self.C1[i].place(x=73,y= 220 + (20*i))
           
            self.buttonStyle[i] = tkinter.Button(self.histogramFrame, text=self.styleName[i], image=self.pixelVirtual, width=int((self.mywidth / 100) * 0.88), height=int((self.myheight / 100) * 1.17), compound=tkinter.CENTER, command = lambda ind=i: self.changeStyle(ind))
            self.buttonStyle[i].place(x=103, y= 220 + (20*i))
            
            print(self.animation)
            if(self.animation==True):
                self.labelAnalyze[i] = tkinter.Label(self.histogramFrame, text=f'{self.grafY[i][-1]:.3f}', background='white', width=int((self.mywidth / 100) * 1)) 
                self.labelAnalyze[i].place(x=140, y=220 + (20*i))
                print("ahoj")
            else:
                self.buttonAnalyze[i] = tkinter.Button(self.histogramFrame, text="Analyze", image=self.pixelVirtual, width=int((self.mywidth / 100) * 4), height=int((self.myheight / 100) * 1.17), compound=tkinter.CENTER, command = lambda ind=i: self.analyze(ind))
                self.buttonAnalyze[i].place(x=145, y=220 + (20*i))     
    
    def adjust(self):
        self.zoomed = False
        self.adjustView()
    
    def adjustView(self):
        self.graph_x = self.grafX[0]    
            
        self.x_line = [0] * len(self.graph_x)
        self.x_line = self.graph_x        
       
        
        self.graph_yAdjust = {}
        
        self.adjustMin = [float("inf")] * (self.axes-1)
        self.adjustMax = [-float("inf")] * (self.axes-1)
        
        self.check = 0
      
        lenght = len(self.ax1.lines)
        self.calculateData()
        for k in range(lenght):
            if(k < len(self.ax1pos)):
                if(self.vis[self.ax1pos[k]].get() == True):
                    continue
                
            else:
               if(self.vis[self.axes].get() == True):
                   continue
            
            self.check = 1
               
            self.indexAdjust = float(self.xminEntry)
            self.indexAdjust = self.find_nearest(self.grafX[self.ax1pos[k]], self.indexAdjust)
            self.indexAdjust = self.grafX[self.ax1pos[k]].index(self.indexAdjust)

            self.indexAdjust2 = float(self.xmaxEntry)
            self.indexAdjust2 = self.find_nearest(self.grafX[self.ax1pos[k]], self.indexAdjust2)
            self.indexAdjust2 = self.grafX[self.ax1pos[k]].index(self.indexAdjust2)
           
                
            self.graph_yAdjust[self.ax1pos[k]] = self.grafY[self.ax1pos[k]][self.indexAdjust:self.indexAdjust2]

            self.adjustMin[self.ax1pos[k]] = min(self.graph_yAdjust[self.ax1pos[k]])
            self.adjustMax[self.ax1pos[k]] = max(self.graph_yAdjust[self.ax1pos[k]])
        
        if(self.check == 1 and self.zoomed == False):
        
           self.y1minEntry = min(self.adjustMin)
           self.y1maxEntry = max(self.adjustMax)
            
        self.adjustMin2 = [float("inf")] * (self.axes-1)
        self.adjustMax2 = [-float("inf")] * (self.axes-1)
        
        self.check2 = 0
       
        
        self.userZoom()
    
    def find_nearest(self, array, value):
        array = np.asarray(array)
        idx = (np.abs(array - value)).argmin()
        return array[idx]
    
    def analyze(self, index):
        if(self.dataShow == 1):
            self.analyzeFrame.destroy()
            
        self.dataShow = 0
        for l in range(0, self.axes-1):
            if(l == index):
                if(l == self.axes):
                    self.vis[l].set(True)
                    
                else:
                    self.vis[l].set(False)
                    
                    
                continue
            self.vis[l].set(True)
        self.setVisibility()
        
        self.readINI()
        self.readMetod(self.config["METODIKA"]["file"])
        
        plt.title(self.configFileData[self.val][1])
        self.canvas.draw()

        
    def checkAxes(self):
        self.visible = 0
        self.val = 0
        
        for k in range(0, self.axes - 1):
            if(self.vis[k].get() == False):
                self.visible += 1
                self.val = k
             
        return self.visible
    
    def analyzeDataWindow(self, ind):
        self.analyzeFrame = tkinter.Frame(self.histogramFrame, width = 300, height = self.myheight)
        self.analyzeFrame.place(x = 0, y = (220+(20*self.endpoint)))
        
        self.scrollbar.config(orient=tkinter.VERTICAL, command=self.canvas2.yview)
        self.canvas2.configure(yscrollcommand = self.scrollbar.set)
        self.canvas2.bind('<Configure>', self.on_configure)
        
        self.ResultsButton = tkinter.Button(self.analyzeFrame, text="Window", image=self.pixelVirtual, width=35, height=10, compound=tkinter.CENTER, command=lambda l = self.val: self.results(l))
        self.ResultsButton.place(x=5, y= 0)
        
        #self.reportButton = tkinter.Button(self.analyzeFrame, text="Report", image=self.pixelVirtual, width=30, height=10, compound=tkinter.CENTER, command = self.report)
        #self.reportButton.place(x = 135, y = 225) 
        
        if(self.dataShow == 0):
            self.results(ind)
        
        self.dataShow = 1
    
    
    def convert(self):
        if(self.baselineCheck.get() == True):
            self.base = np.average(self.grafY[self.val][:500])
                
            for r in range(len(self.grafY[self.val])):
                self.grafY[self.val][r] -= self.base
                
            self.updateChart()
                
            self.baselineCheck.set(False)
        
        else:
            
            for r in range(len(self.grafY[self.val])):
                if(self.h.get() == '+'):
                    self.grafY[self.val][r] += float(self.convertEntry.get())
                    
                elif(self.h.get() == '-'):
                    self.grafY[self.val][r] -= float(self.convertEntry.get())
                
                elif(self.h.get() == '*'):
                    self.grafY[self.val][r] *= float(self.convertEntry.get())
                
                elif(self.h.get() == '/'):
                    self.grafY[self.val][r] /= float(self.convertEntry.get())
                
                    
                self.updateChart()
            
            else:
                for r in range(len(self.graphy)):
                    if(self.h.get() == '+'):
                        self.graphy[r] += float(self.convertEntry.get())
                        
                    elif(self.h.get() == '-'):
                        self.graphy[r] -= float(self.convertEntry.get())
                        
                    elif(self.h.get() == '*'):
                        self.graphy[r] *= float(self.convertEntry.get())
                        
                    elif(self.h.get() == '/'):
                        self.graphy[r] /= float(self.convertEntry.get())
    
    
    def convertx(self):
      
        for r in range(len(self.graph_x)):
            if(self.h2.get() == '+'):
                self.graph_x[r] += float(self.convertEntry2.get())
                
            elif(self.h2.get() == '-'):
                self.graph_x[r] -= float(self.convertEntry2.get())
            
            elif(self.h2.get() == '*'):
                self.graph_x[r] *= float(self.convertEntry2.get())
            
            elif(self.h2.get() == '/'):
                self.graph_x[r] /= float(self.convertEntry2.get())

        self.xminEntry = min(self.graph_x)
        self.xmaxEntry = max(self.graph_x)
        
        self.updateChart()
    
    def unboldButtons(self):
        self.ResultsButton.configure(font="Helevtica 10")
    
    def results(self, ind):  
        self.dataShow = 1
        self.textLabel = ["Max:", "Min:", "Avg:", "P-P:", "SD:", "XRange:", "XCount:"]
        self.dataBox = Text(self.analyzeFrame, width=29, height=7, font="Halvetica 11")
        self.dataBox.tag_configure("bold", font="Helvetica 11 bold")
        self.dataBox.tag_configure("notbold", font="Helvetica 11")
        
       
        if(self.default == 0):
   
            self.x_line = [0] * len(self.graph_x)
            self.x_line = self.graph_x
            
            self.indexAdjust = float(self.xminEntry)
            self.indexAdjust = self.find_nearest(self.x_line, self.indexAdjust)
            self.indexAdjust = self.x_line.index(self.indexAdjust)
            
            self.indexAdjust2 = float(self.xmaxEntry)
            self.indexAdjust2 = self.find_nearest(self.x_line, self.indexAdjust2)
            self.indexAdjust2 = self.x_line.index(self.indexAdjust2)
            
            self.adjustMin = [float("inf")] * (self.axes-1)
            self.adjustMax = [-float("inf")] * (self.axes-1)
                        
            self.graph_yAdjust[ind] = self.grafY[ind][self.indexAdjust:self.indexAdjust2]
               
            
            self.textfield = [max(self.graph_yAdjust[ind]), min(self.graph_yAdjust[ind]), np.average(self.graph_yAdjust[ind]), max(self.graph_yAdjust[ind]) - min(self.graph_yAdjust[ind]), np.std(self.graph_yAdjust[ind]), float(self.xmaxEntry) - float(self.xminEntry), len(self.graph_yAdjust[ind])]
            self.average = np.average(self.graph_yAdjust[ind])
            self.SD = np.std(self.graph_yAdjust[ind])
        
        else:
            self.average = np.average(self.grafY[ind])
            self.SD = np.std(self.grafY[ind])
            self.textfield = [self.y_max[ind], self.y_min[ind], self.average, self.y_max[ind] - self.y_min[ind], self.SD, max(self.graph_x) - min(self.graph_x), len(self.grafX[ind])]
       
        for l in range(0, len(self.textfield)):
            self.text = self.textfield[l]
            pos = str(l+1) + ".0" 
            self.dataBox.insert(str(pos), self.textLabel[l] + "\t", "bold")
            pos = str(l+1) + ".10"
            
            
            if(float(self.text) < 1):
                self.dataBox.insert(str(pos), str("{:.3}".format(float(self.text))) + "\n", "notbold")
            else:
                self.dataBox.insert(str(pos), str("{:.3f}".format(float(self.text))) + "\n", "notbold")
        
        
        self.dataBox.configure(state=DISABLED)
        self.dataBox.place(x = 5, y = 22)
        
        self.dataBox.bind("<1>", lambda event: self.dataBox.focus_set())
        
        self.unboldButtons()
        self.ResultsButton.configure(font="Halevtica 10 bold")
        
        self.convertLabel = tkinter.Label(self.analyzeFrame, text = "Convert:", font="Halevtica 10 bold")
        self.convertLabel.place(x = 5, y = 155)
        
        self.baselineCheck = BooleanVar()

        
        self.h = tkinter.StringVar() 
        self.h.set('+')
        self.operation = ttk.Combobox(self.analyzeFrame, width = 2, textvariable = self.h) 
        
       
        self.operation['values'] = ('+', "-", "*", "/") 
        bonus = 10
        
        self.operation.place(x = 18, y = 165 + bonus) 
        self.operation.current() 
        
        self.convertEntry = tkinter.Entry(self.analyzeFrame, width = 10)
        self.convertEntry.place(x = 50, y = 164+ bonus)
        
        self.convertButton = tkinter.Button(self.analyzeFrame, text="Ok", image=self.pixelVirtual, width=10, height=10, compound=tkinter.CENTER, command = self.convert)
        self.convertButton.place(x = 135, y = 163+ bonus)
        
    
        self.h2 = tkinter.StringVar() 
        self.h2.set('+')
        self.operation2 = ttk.Combobox(self.analyzeFrame, width = 2, textvariable = self.h2) 
          
       
        self.operation2['values'] = ('+', "-", "*", "/") 
          
        self.operation2.place(x = 18, y = 190+ bonus) 
        self.operation2.current() 
        
        self.convertEntry2 = tkinter.Entry(self.analyzeFrame, width = 10)
        self.convertEntry2.place(x = 50, y = 189+ bonus)
        
        self.convertButton2 = tkinter.Button(self.analyzeFrame, text="Ok", image=self.pixelVirtual, width=10, height=10, compound=tkinter.CENTER, command = self.convertx)
        self.convertButton2.place(x = 135, y = 188+ bonus)
        
        yLabel = tkinter.Label(self.analyzeFrame, text = "Y:")
        yLabel.place(x = 0, y = 166+ bonus)
        
        
        xLabel = tkinter.Label(self.analyzeFrame, text = "X:")
        xLabel.place(x = 0, y = 191+ bonus)
        
    
        #self.convertLabel = tkinter.Label(self.analyzeFrame, text = "Derivation", font="Halevtica 10")
        #self.convertLabel.place(x = 35, y = 227)
        
        
        self.derivationEntry = tkinter.Entry(self.analyzeFrame, width = 10)
        self.derivationEntry.insert(0, "auto")
        self.derivationEntry.place(x = 35, y = 227)
        
        
        self.derivationCheck = BooleanVar()
        self.derivationCheckbox = tkinter.Checkbutton(self.analyzeFrame, variable = self.derivationCheck, command = self.Derivace)
        self.derivationCheckbox.place(x = 10, y = 225)

        
        
        self.reportButton = tkinter.Button(self.analyzeFrame, text="Report", image=self.pixelVirtual, width=30, height=10, compound=tkinter.CENTER, command = self.report)
        self.reportButton.place(x = 135, y = 225) 

        
        self.canvas2.bind('<Configure>', self.on_configure)
        self.canvas.draw()
        
        #self.histogramFrame.configure(height = self.myheight*2)
        
        
    
    def report(self):
        with PdfPages(r'reports/'+ self.timestamp +'.pdf') as export_pdf:
            
            plt.figure() 
            plt.axis('off')
            plt.text(0.5,0.6,"Protokol analýzy", fontsize = 30, fontweight = "bold", ha='center',va='center')
            plt.text(0.5,0.4,"ChromaDLo v1.0", fontsize = 25, ha='center',va='center')
            export_pdf.savefig()
            plt.close()
            
            
            
            export_pdf.savefig()
            
            fig, ax = plt.subplots(figsize =(11.7,8.27))
            
            fig.patch.set_visible(False)
            ax.axis('off')
            ax.axis('tight')
            
            self.readINI()
            self.readMetod(self.config["METODIKA"]["file"])
            data = self.configFileData
            print(data)
            
            
            t15_data = [["Vzorek",self.VzorekEntry.get()]]
            t1_data = [["Metodika",data[5][1]], ["Laborator",data[5][2]], ["Laborant",data[5][3]],["Tolerance",data[6][1]],["Objem\n nastriku",data[6][2]],["Datum",self.mytime],["Delka\n mereni",data[6][0]]]
            t2_data = [["Nazev",data[self.val][1]], ["Filtr",data[self.val][2]], ["Zesileni",data[self.val][3]], ["Rychlost",data[self.val][4]]]
            
            t3_data = []
            for i in range(len(self.maxX)):
                t3_data.append([self.maxX[i], np.round(self.integral[i], 4)])            
            
            pocet = len(t3_data)
            print(pocet, "tohle je pocet ", t3_data)

            row3 = ['Peek%d' % x for x in range(1,pocet+1)]
            
            text = data[5][4]
            
            for i in range(len(text)):
                if((i % 45) == 0):
                    text = text[:i] + "\n" + text[i:]
                    
            text2 = self.PopisEntry.get("1.0", END)
            
            for i in range(len(text)):
                if((i % 45) == 0):
                    text2 = text2[:i] + "\n" + text2[i:]
            
            plt.text(0.5, 1.15, "Metodika", color="black",ha="center", fontsize = 15, fontweight = "bold")
            
            t1 = plt.table(cellText=t1_data,loc='center', cellLoc = 'center',colWidths = [0.15,0.85], bbox=[0, 0.15, 1, 0.5]) 
            for i in range(len(t1_data)):
                t1[(i,0)].set_facecolor("#56b5fd")
                
            t15 = plt.table(cellText=t15_data,loc='center', cellLoc = 'center', colWidths = [0.15,0.85], bbox=[0,0.8,1,0.05])   
            for i in range(len(t15_data)):
                t15[(i,0)].set_facecolor("#56b5fd")
                
            table2 = plt.table(cellText=[[text2]], loc='center', cellLoc =  'center', bbox=[0.12,0.65,0.88,0.15])
            tablePopis2 = plt.table(cellText=[[" Popis \nVzorku"]], loc='center', cellLoc = 'center', colLoc = "top", bbox=[0,0.65, 0.15, 0.15])
            tablePopis2[(0,0)].set_facecolor("#56b5fd")
            
            table = plt.table(cellText=[[text]], loc='center', cellLoc = 'center',                              bbox=[0.12, 0, 0.88, 0.15]) 
            tablePopis = plt.table(cellText=[["Popis"]], loc='center', cellLoc = 'center', colLoc = "top",      bbox=[0,0, 0.15, 0.15])
            tablePopis[(0,0)].set_facecolor("#56b5fd")
            
            export_pdf.savefig()
            plt.close()
            
            fig, ax = plt.subplots()
            
            fig.patch.set_visible(False)
            ax.axis('off')
            ax.axis('tight')
            
            plt.text(0.5, 1.1, "Nastavení ADC", color="black",ha="center", fontsize = 15, fontweight = "bold")
            
            t2 = plt.table(cellText=t2_data, loc='center', cellLoc = 'center',colWidths = [0.15,0.85], bbox=[0, 0.7, 1, 0.25]) 
            for i in range(len(t2_data)):
                t2[(i,0)].set_facecolor("#56b5fd")
            
            [t.auto_set_font_size(False) for t in [t1, t2, table, tablePopis]]
            
            export_pdf.savefig()
            plt.close()
            
            fig, ax = plt.subplots()
            
            fig.patch.set_visible(False)
            ax.axis('off')
            ax.axis('tight')
            plt.text(0.5, 1.1, "Výsledky analýzy", color="black",ha="center", fontsize = 15, fontweight = "bold")
            
            t3 = plt.table(cellText=t3_data, loc='center', rowLabels=row3,cellLoc = 'center', bbox=[0.125, 0, 0.875, 0.9], colWidths = [0.4375,0.4375])
            t3rows = plt.table(cellText=[["Retenční čas","Integrál"]], loc='center', cellLoc = 'center', bbox=[0.125, 0.9, 0.875, 0.07]) 
            t3rows[(0,0)].set_facecolor("#56b5fd")
            t3rows[(0,1)].set_facecolor("#56b5fd")    
            
            
            print(pocet)
            for i in range(pocet):
                t3[(i,-1)].set_facecolor("#56b5fd")
                   
            
            [t.auto_set_font_size(False) for t in [t1, t2, t3, table, tablePopis]]
            export_pdf.savefig()
            plt.close()
        
        config = []
        f = open('reports/'+self.timestamp +'.csv', "w")
        
        self.readMetod("Metodika_generator_test1.csv")
        data = self.configFileData
        print(data)
        
        cistePopisy = self.PopisEntry.get("1.0",END).replace("\n"," ")
        print(cistePopisy)
        
        config.append(["METODIKA", "\n", "Vzorek",";",self.VzorekEntry.get(),"\n","Popis vzorku",";", cistePopisy,"\n"])
        config.append(["Metodika",";",data[5][1],"\n","Laborator",";",data[5][2],"\n", "Laborant",";", data[5][3],"\n", "Tolerance",";",data[6][1],"\n","Objem nastriku",";", data[6][2],"\n","Datum",";",self.mytime,"\n","Delka mereni",";", data[6][0], "\n","Popis metodiky",";", data[5][4], "\n","\n"]) 
        
        config.append(["NASTAVENI ADC","\n"])
        config.append(["Nazev kanalu",";",data[self.val][1],"\n","Filtr",";",data[self.val][2], "\n","Zesileni",";",data[self.val][3],"\n","Rychlost",";",data[self.val][4],"\n","\n"])
        
        
        config.append(["VYSLEDKY ANALYZY","\n","",";","Retencni cas",";","Integral","\n"])
        print(pocet,self.integral,self.maxX)
        for x in range(pocet):
            config.append([("Peek"+str(x+1)),";",self.maxX[x],";", self.integral[x],"\n"])
        
        #config.append
        print(config)
        
        for radky in range(len(config)):
             for sloupce in range(len(config[radky])):
                 f.write(str(config[radky][sloupce]))
                 
        f.close()
        
    def ctrlP(self,event):
        self.screenshot()
    
    def screenshot(self):
        legendlist = [0] * (self.axes-1)
        self.readINI()
        self.readMetod(self.config["METODIKA"]["file"])

        
        self.fig.canvas.toolbar.save_figure()
        self.canvas.draw()
    
    def changeStyle(self, i):
        self.point = True
        if(self.styleName[i] == "Point"):
            self.styleName[i] = "Line"
            self.currStyle = "Line"
            self.style[i] = "-"
            self.buttonStyle[i].configure(text='Line')
            self.derivationCheck = 0
            
        else:
            self.styleName[i] = "Point"
            self.currStyle = "Point"
            self.style[i] = "."
            self.buttonStyle[i].configure(text='Point')
            self.derivationCheck = 0
        self.updateChart()
    
    def colorPicker(self, ind):
        self.colorWindow = tkinter.Toplevel(root)
        
        self.colorWindow.title()
        self.PickerColor = ["#ff0000", "#00cc00", "#000066", "#ffff00", "#ff9900", "#ff66ff", "#cc0099", "black", "#cccccc", "#00ffff"]
        for k in range(10):
            Button(self.colorWindow, bg=self.PickerColor[k], command = lambda i=k, l = ind: self.readColor(i, l)).grid(row = 0, column = k)
    
    def setVisibility(self):
        self.zoomed = False
        self.setVis()
    
    def setVis(self):              
        for l in range(0, self.axes - 1):
            if(self.vis[l].get() != self.checkVis[l]):
                self.checkVis[l] = self.vis[l].get()
                for i,j in enumerate(self.ax1.lines):
                    if((len(self.ax1pos)) > i):
                        if(self.ax1pos[i] == l):
                            if(j.get_visible() == False):
                                j.set_visible(True)
                            else:
                                j.set_visible(False)
            
        
        self.adjustView()
        
        
        if(self.checkAxes() == 1):
            if(self.dataShow == 0):
                self.analyzeDataWindow(self.val)
        
        elif(self.checkAxes() == 0):
            if(self.dataShow == 0):
                self.adjustView()
                self.analyzeDataWindow(self.axes)
        
        else:
            if(self.dataShow == 1):
                self.analyzeFrame.destroy()
        
        self.dataShow = 0
        self.point = False

    
    def defaultView(self):
        self.default = 1
    
        self.x_line = [0] * len(self.graph_x)
        self.x_line = self.graph_x
        
    def userZoom(self):
        self.direct = 0
        self.zoomXmin = float(self.xminEntry)        
        self.zoomXmax = float(self.xmaxEntry)
        
        self.zoomY1min = float(self.y1minEntry)
        self.zoomY1max = float(self.y1maxEntry)

        self.ax1.set_xlim(self.zoomXmin, self.zoomXmax)
        self.ax1.set_ylim(self.zoomY1min, self.zoomY1max)

        self.chartSpace()
        self.canvas.draw()
        
        
    def Derivace(self):
        self.zacatek = [0] * 100
        self.konec = [0] * 100
        self.zacatekPicker = [0] * 100
        self.konecPicker = [0] * 100
        self.konecPickerX = [0] * 100
        self.zacatekPickerX = [0] * 100
        self.konecPickerX2 = [0] * 100
        self.zacatekPickerX2 = [0] * 100
        self.lastcycle = 0
        self.activation2 = True
        
        tol = 0
        self.ys = np.diff(self.grafY[self.val][tol:]) / np.diff(self.grafX[self.val][tol:])
        self.xs = (np.array(self.grafX[self.val])[tol:-1] + np.array(self.grafX[self.val])[tol+1:]) / 2 
        #self.ax2.plot(self.xs, self.ys, ".")
        #self.ax2.set_ylim(min(self.ys),max(self.ys))
        
        self.tolerance = 4
        k = ((max(self.ys) - min(self.ys)) / 100) * 1
        nad = ((max(self.ys) - min(self.ys)) / 100) * self.tolerance
        
        start = 0
        self.ys = self.ys.tolist()
        
        nadlimit = np.where((self.ys < k*1) & (self.ys > -k*1))[0]
        
        cyclenum = 0
        
        self.setPickerButton = tkinter.Button(self.analyzeFrame, text="Set derivation", image=self.pixelVirtual, width=70, height=10, compound=tkinter.CENTER, command = self.SetPicker)
        self.setPickerButton.place(x = 15, y =  255)
        
        self.fig.canvas.toolbar.zoom() 
        
        #while(start < len(self.grafY[self.val])):
        
        if(self.derivationEntry.get() == "auto"):
          
            while(start < len(self.grafY[self.val])):
                self.zacatek[cyclenum] = [x for x in self.ys[start:] if x > nad][0]
                self.zacatek[cyclenum] = self.ys.index(self.zacatek[cyclenum],start)
                print(self.zacatek[cyclenum])
                
                self.konec[cyclenum] = [x for x in nadlimit if x > self.zacatek[cyclenum]][1] #[x for x in self.grafY[self.val][self.zacatek:] if x < self.grafY[self.val][self.zacatek]+k*1 and x > self.grafY[self.val][self.zacatek]- k*1][0] #
                
                konec2 = [x for x in self.grafY[self.val][self.zacatek[cyclenum]:] if x < self.grafY[self.val][self.zacatek[cyclenum]]]
                if(len(konec2) == 0):
                    konec2 = self.konec[cyclenum]
                else:
                    konec2 = konec2[0]
                    konec2 = self.grafY[self.val].index(konec2,self.zacatek[cyclenum])
                
                
                print(self.konec[cyclenum], konec2)
                if(konec2 < self.konec[cyclenum]):
                    self.konec[cyclenum] = konec2
                
                self.zacatekPickerX[cyclenum] = self.grafX[self.val][self.zacatek[cyclenum]]
                self.konecPickerX[cyclenum]=  self.grafX[self.val][self.konec[cyclenum]]
                
                self.zacatekPicker[cyclenum] = plt.axvline(x=self.zacatekPickerX[cyclenum], c="black", lw = 1, picker=5, label = 1000+cyclenum)
                self.konecPicker[cyclenum] = plt.axvline(x=self.konecPickerX[cyclenum], c="black", lw = 1, picker=5, label = 2000+cyclenum)
                
                self.canvas.draw()
                
                start = self.konec[cyclenum]
                cyclenum = cyclenum+1
                self.lastcycle = cyclenum
                
        else:
            for cy in range(int(self.derivationEntry.get())):
                self.zacatek[cyclenum] = [x for x in self.ys[start:] if x > nad][0]
                self.zacatek[cyclenum] = self.ys.index(self.zacatek[cyclenum],start)
                print(self.zacatek[cyclenum])
                
                self.konec[cyclenum] = [x for x in nadlimit if x > self.zacatek[cyclenum]][1] #[x for x in self.grafY[self.val][self.zacatek:] if x < self.grafY[self.val][self.zacatek]+k*1 and x > self.grafY[self.val][self.zacatek]- k*1][0] #
                
                konec2 = [x for x in self.grafY[self.val][self.zacatek[cyclenum]:] if x < self.grafY[self.val][self.zacatek[cyclenum]]]
                if(len(konec2) == 0):
                    konec2 = self.konec[cyclenum]
                else:
                    konec2 = konec2[0]
                    konec2 = self.grafY[self.val].index(konec2,self.zacatek[cyclenum])
                
                
                print(self.konec[cyclenum], konec2)
                if(konec2 < self.konec[cyclenum]):
                    self.konec[cyclenum] = konec2
                
                self.zacatekPickerX[cyclenum] = self.grafX[self.val][self.zacatek[cyclenum]]
                self.konecPickerX[cyclenum]=  self.grafX[self.val][self.konec[cyclenum]]
                
                self.zacatekPicker[cyclenum] = plt.axvline(x=self.zacatekPickerX[cyclenum], c="black", lw = 1, picker=5, label = 1000+cyclenum)
                self.konecPicker[cyclenum] = plt.axvline(x=self.konecPickerX[cyclenum], c="black", lw = 1, picker=5, label = 2000+cyclenum)
                
                self.canvas.draw()
                
                start = self.konec[cyclenum]
                cyclenum = cyclenum+1
                self.lastcycle = cyclenum
            
            
    def SetPicker(self):
        self.activation = False
        self.activation2 = False
        print(self.lastcycle)
        i = 0
        while(i < self.lastcycle):
           
            self.zacatekPickerX2[i] = min(self.grafX[self.val], key=lambda x:abs(x-self.zacatekPickerX[i]))
            self.zacatekPicker[i].remove()
            
            self.konecPickerX2[i] = min(self.grafX[self.val], key=lambda x:abs(x-self.konecPickerX[i]))
            self.konecPicker[i].remove()

            self.zacatek[i] = self.grafX[i].index(self.zacatekPickerX2[i])
            self.konec[i] = self.grafX[i].index(self.konecPickerX2[i])
            
            
            
            dx = self.grafX[self.val][self.konec[i]] - self.grafX[self.val][self.zacatek[i]]
            
            k = (self.grafY[self.val][self.konec[i]] - self.grafY[self.val][self.zacatek[i]]) / (self.grafX[self.val][self.konec[i]] - self.grafX[self.val][self.zacatek[i]])
            q = self.grafY[self.val][self.zacatek[i]] - (k * self.grafX[self.val][self.zacatek[i]])

    
            baselineSum = []

            for l in range(len(self.grafX[self.val][self.zacatek[i]:self.konec[i]])):
                baselineSum.append(((k * self.grafX[self.val][self.zacatek[i] + l]) + q))
                
          
            dy = np.subtract(self.grafY[self.val][self.zacatek[i]:self.konec[i]],baselineSum)
            
            dy = np.sum(dy)
            
            self.integral.append(dy/dx)
            print(self.integral, "integral")
            
            print(self.zacatek[i],self.konec[i])
            index = self.grafY[self.val].index(max(self.grafY[self.val][self.zacatek[i]:self.konec[i]]))
            print("index", index)
            maxY = max(self.grafY[self.val][self.zacatek[i]:self.konec[i]])
            print(maxY)
            
            self.maxX.append(self.grafX[self.val][self.grafY[self.val].index(maxY, self.zacatek[i], self.konec[i])])
            print(maxY, self.maxX[i])
            
            maxY2 = max(self.grafY[self.val][self.zacatek[i]:self.konec[i]]) - baselineSum[index - self.zacatek[i]]
            
            rangeY = (self.ax1.get_ylim()[1]-self.ax1.get_ylim()[0])/100
            rangeX = (self.ax1.get_xlim()[1]-self.ax1.get_xlim()[0])/100
            
            if(self.grafY[self.val][self.zacatek[i]] < self.grafY[self.val][self.konec[i]]):
                fillval = self.grafY[self.val][self.konec[i]]
            else:
                fillval = self.grafY[self.val][self.zacatek[i]]
            print(fillval)
            
            
            #print(self.integral[i],"halo")
            
            print(baselineSum[0], baselineSum[-1])
         
            self.ax1.fill_between(self.grafX[self.val][self.zacatek[i]:self.konec[i]], self.grafY[self.val][self.zacatek[i]:self.konec[i]], baselineSum, color=self.colors[self.val], alpha = 0.6)
           
            #self.ax1.text(self.grafX[self.val][self.zacatek[i]]+rangeX*0.1,rangeY*1, str(np.round(self.integral[i], 4)), style ='italic', fontsize = 10, color ="black")
            
            #self.ax1.annotate('ahojte', xy=(self.grafX[self.val][self.zacatek[i]]+rangeX*0.1,rangeY*1), textcoords='data')
            
            self.ax1.annotate('%s' % str(np.round(self.integral[i], 4)), xy=(self.maxX[i], baselineSum[0]), textcoords='data')
            
            self.ax1.annotate('%s (%s)' % (self.maxX[i], str(round(maxY2, 3))), xy=(self.maxX[i],maxY+rangeY*0.5), textcoords='data')
            print("halo2") 
            self.canvas.draw()
            i =i+1
            
    def onpick(self, event): 
        if(self.activation2 == True):
            art = event.artist
            self.code = int(event.artist.properties()['label'])
            print(self.code)
            self.activation = True
            
    def onmotion(self, event):  
        if(self.activation == True):
            if(event.xdata != None and event.ydata != None):
                self.motion = True
                
                if(self.code < 2000):
                    
                    self.zacatekPicker[self.code-1000].remove()
                    self.zacatekPicker[self.code-1000] = plt.axvline(x=event.xdata, c="black", lw = 1, picker=5, label = self.code)
                    self.zacatekPickerX[self.code-1000] = event.xdata
                else:
                    self.konecPicker[self.code-2000].remove()
                    self.konecPicker[self.code-2000] = plt.axvline(x=event.xdata, c="black", lw = 1, picker=5, label = self.code)
                    self.konecPickerX[self.code-2000] = event.xdata
                
                self.canvas.draw()
                
    def onrelease(self,event):
        self.activation = False
    
    # ******************************************** Read and write to INI file ********************************************
    def writeINI(self):
         with open("status.ini", "w") as configfile:
             self.config.write(configfile)
             configfile.close()
    
    
    def readINI(self):
        self.config = configparser.ConfigParser()
        self.config.sections()
        self.config.read('status.ini')
    
    # ******************************************** Keyboard shortcuts callbacks ********************************************
    def ctrlX(self, event):
        os.execl(sys.executable, sys.executable, *sys.argv)
    
    def ctrlm(self, event):
        self.Metodika()
    
    def ctrlO(self, event):
        
        self.animation = False
        self.csvUpload()
    
    def ctrlC(self, event):
        root.clipboard_clear()
        root.clipboard_append(f'{self.mouse_x}\t{self.mouse_y}')
        
    def ctrlD(self, event):
        self.fig.canvas.toolbar.configure_subplots()
    
    def shift(self, event):
        print("shift")
        self.pan = True
        self.fig.canvas.toolbar.pan()
            
    def shift_release(self, event):
        
        print("shift pusteny")
        self.fig.canvas.toolbar.zoom()
            
        self.pan = False
    
    # ******************************************** Functions for the Metodika toplevel ********************************************    
    def openMet(self):
        print("Hello")
        self.clearMet()
        self.openM= filedialog.askopenfilename()
        self.readMetod(self.openM)
    
    def updateScrollRegion(self):
        self.cTableContainer.update_idletasks()
        self.cTableContainer.config(scrollregion=self.metodikaLevel.bbox())
    
    def updateScrollRegion2(self):
        self.canvas2.update_idletasks()
        self.canvas2.config(scrollregion=self.histogramFrame.bbox())
    
    def readMetod(self, filename):        
        self.configFileData = []
        self.prerow = self.rownum
        self.rownum = 0
        self.configFileData = self.readcsv(filename)
        self.rownum = self.prerow
    
    def setMetod(self,filename):
        self.readMetod(filename)
        for x in range(5):
            self.CHNazev[x].insert(0,self.configFileData[x][1])
            if(self.configFileData[x][0] == "True"):
                self.CHCheck[x].set(True)
            else:
                self.CHCheck[x].set(False)
            
            self.CHFilter[x].insert(0,self.configFileData[x][2])
            self.CHGain[x].insert(0,self.configFileData[x][3])
            self.CHRate[x].insert(0,self.configFileData[x][4])
                
        self.MetNazevMetodikaEntry.insert(0,self.configFileData[5][1])
        self.MetNazevLaboratorEntry.insert(0,self.configFileData[5][2])
        self.MetJmenoLaborantEntry.insert(0,self.configFileData[5][3])
        self.MetPopisEntry.insert(tkinter.END, self.configFileData[5][4])
        self.MetDelkaEntry.insert(0,self.configFileData[6][0])
        self.MetToleranceEntry.insert(0,self.configFileData[6][1])
        self.MetNastrikEntry.insert(0,self.configFileData[6][2])
        
        
    def clearMet(self):
        for x in range(0,6):
            self.CHNazev[x+1].delete(0,"end")
            self.CHFilter[x].delete(0,"end")
            self.CHGain[x].delete(0,"end")
            self.CHRate[x].delete(0,"end")

        self.MetNazevMetodikaEntry.delete(0,"end")
        self.MetJmenoLaborantEntry.delete(0,"end")
        self.MetToleranceEntry.delete(0,"end")
        self.MetDelkaEntry.delete(0,"end")
        self.MetNastrikEntry.delete(0,"end")
        
    def Metodika(self):
        bg = "white"
        velikostF = 15
        self.const = int((root.winfo_screenwidth()))
        
        self.metodikaLevel0 = tkinter.Toplevel(bg = bg)
        self.metodikaLevel0.geometry("{0}x{1}+0+0".format(self.mywidth, self.myheight))
        self.metodikaLevel0.title("Metodika")
        
        self.cTableContainer = tkinter.Canvas(self.metodikaLevel0)
        self.metodikaLevel = tkinter.Frame(self.cTableContainer, bg =bg)
        sbVerticalScrollBar = tkinter.Scrollbar(self.metodikaLevel0)
        
        self.cTableContainer.config(yscrollcommand=sbVerticalScrollBar.set, highlightthickness=0)
        sbVerticalScrollBar.config(orient=tkinter.VERTICAL, command=self.cTableContainer.yview)
        
        sbVerticalScrollBar.pack(fill=tkinter.Y, side=tkinter.RIGHT, expand=tkinter.FALSE)
        self.cTableContainer.pack(fill=tkinter.BOTH, side=tkinter.LEFT, expand=tkinter.TRUE)
        self.cTableContainer.create_window(0, 0, window=self.metodikaLevel, anchor=tkinter.NW)
        i=0
        

        self.MetMereniNadpisFrame = tkinter.Frame(self.metodikaLevel, bg =bg)
        self.MetMereniNadpisFrame.grid(row = 0, column = 0)
        
        self.MetMereniFrame = tkinter.Frame(self.metodikaLevel, bg = bg)
        self.MetMereniFrame.grid(row = 1, column = 0)
        
        self.MetADCNadpisFrame = tkinter.Frame(self.metodikaLevel, bg =bg)
        self.MetADCNadpisFrame.grid(row = 2, column = 0)
        
        self.MetADCFrame = tkinter.Frame(self.metodikaLevel, bg =bg)
        self.MetADCFrame.grid(row = 3, column = 0)
        
        self.MetTabulkaNadpisFrame = tkinter.Frame(self.metodikaLevel, bg =bg)
        self.MetTabulkaNadpisFrame.grid(row = 4, column = 0)
        
        self.MetTabulkaFrame = tkinter.Frame(self.metodikaLevel, bg =bg)
        self.MetTabulkaFrame.grid(row = 5, column = 0)
    
        #################################################### Metodika mereni frame ####################################################
        self.img = Image.open("metodika_mereni.png")
        self.width2, self.height2 = self.img.size
        self.img = self.img.resize((self.const, int((self.const / self.width2) * self.height2)), Image.ANTIALIAS)
        photo = ImageTk.PhotoImage(self.img)            
        
        self.MetNadpis = tkinter.Label(self.MetMereniNadpisFrame, image = photo, bg = bg)
        self.MetNadpis.grid(row = 0, column = 0, sticky = "e", columnspan = 50)
        self.MetNadpis.image = photo
        
        self.const = int((root.winfo_screenwidth() / 5.25))
        
        self.img = Image.open("ChromaDLo_nahratbutton.png")
        self.width2, self.height2 = self.img.size
        self.img = self.img.resize((self.const, int((self.const / self.width2) * self.height2)), Image.ANTIALIAS)
        photo = ImageTk.PhotoImage(self.img)
        
        self.buttonMetNahraj = tkinter.Button(self.MetMereniFrame, text="Nahrát metodiku", image=photo, bd = "0", bg = bg, command = self.openMet)
        self.buttonMetNahraj.image = photo
        self.buttonMetNahraj.grid(row = 1, column = 3, columnspan = 7)
        
        self.MetNatevMetkdodika = tkinter.Label(self.MetMereniFrame, text="Název metodiky", bg = bg, font=("Serif", velikostF))
        self.MetNatevMetkdodika.grid(row = 3, column = 1,sticky = "w",columnspan = 2)
        
        self.MetNazevLaborator = tkinter.Label(self.MetMereniFrame, text="Jméno laboratoře", bg = bg, font=("Serif", velikostF))
        self.MetNazevLaborator.grid(row = 4, column = 1,sticky = "w",columnspan = 2)
        
        self.MetJmenoLaborant = tkinter.Label(self.MetMereniFrame, text="Jméno laboranta", bg = bg, font=("Serif", velikostF))
        self.MetJmenoLaborant.grid(row = 5, column = 1,sticky = "w",columnspan = 2)
        
        self.MetPopis = tkinter.Label(self.MetMereniFrame, text="Popis metodiky", bg = bg, font=("Serif", velikostF))
        self.MetPopis.grid(row = 6, column = 1,sticky = "w", columnspan = 2, rowspan=2)
        
        self.MetDelka = tkinter.Label(self.MetMereniFrame, text="Délka měření [min]", bg = bg, font=("Serif", velikostF))
        self.MetDelka.grid(row = 2, column = 8,sticky = "w",columnspan = 2)
        
        self.MetTolerance = tkinter.Label(self.MetMereniFrame, text="Tolerance", bg = bg, font=("Serif", velikostF))
        self.MetTolerance.grid(row = 3, column = 8,sticky = "w",columnspan = 2)
        
        self.MetNastrik = tkinter.Label(self.MetMereniFrame, text="Objem nástřiku", bg = bg, font=("Serif", velikostF))
        self.MetNastrik.grid(row = 4, column = 8,sticky = "w",columnspan = 2)
        
        self.MetNazevMetodikaEntry = tkinter.Entry(self.MetMereniFrame, width = 20)
        self.MetNazevLaboratorEntry = tkinter.Entry(self.MetMereniFrame, width = 20)
        self.MetJmenoLaborantEntry = tkinter.Entry(self.MetMereniFrame, width = 20)
        self.MetPopisEntry = tkinter.Text(self.MetMereniFrame, height=10, width=30)
        self.MetDelkaEntry = tkinter.Entry(self.MetMereniFrame, width = 20)
        self.MetToleranceEntry = tkinter.Entry(self.MetMereniFrame, width = 20)
        self.MetNastrikEntry = tkinter.Entry(self.MetMereniFrame, width = 20)

        self.MetNazevMetodikaEntry.grid(row = 3, column = 4)
        self.MetNazevLaboratorEntry.grid(row = 4, column = 4) 
        self.MetJmenoLaborantEntry.grid(row = 5,  column = 4)
        self.MetPopisEntry.grid(row = 7, column = 4) 
        self.MetDelkaEntry.grid(row = 2, column = 10,sticky = "w",columnspan = 2)
        self.MetToleranceEntry.grid(row = 3, column = 10,sticky = "w",columnspan = 2)
        self.MetNastrikEntry.grid(row = 4, column = 10,sticky = "w",columnspan = 2)
        
        
        self.img = Image.open("ChromaDLo_ulozbutton.png")
        self.width2, self.height2 = self.img.size
        self.img = self.img.resize((self.const, int((self.const / self.width2) * self.height2)), Image.ANTIALIAS)
        photo = ImageTk.PhotoImage(self.img)
        
        self.buttonMetUloz = tkinter.Button(self.MetMereniFrame, text="Uložit metodiku", image=photo, bd = "0", bg = bg, command = self.saveMetod)
        self.buttonMetUloz.image = photo
        self.CH = [0] * 20
        self.buttonMetUloz.grid(row = 8, column = 5, columnspan = 2)
        
        #################################################### Nastaveni ADC frame ####################################################
        self.CHCheck = [0] *  20
        
        self.CHCheckbox = [0] * 20
        self.CHNazev = [0] * 20
        self.vyplnovaciLabel = [0] * 20
        self.CHFilter = [0] * 20
        self.CHGain = [0] * 20
        self.CHRate = [0] * 20
        self.CHFilterV = [0] * 20 
        self.CHGainV = [0] * 20
        self.CHRateV = [0] * 20
        self.CHLabel = [0] * 10
        
        self.const = int((root.winfo_screenwidth()))
        
        self.img = Image.open("nastaveniADC.png")
        self.width2, self.height2 = self.img.size
        self.img = self.img.resize((self.const, int((self.const / self.width2) * self.height2)), Image.ANTIALIAS)
        photo = ImageTk.PhotoImage(self.img)
        
        self.MetNadpis2 = tkinter.Label(self.MetADCNadpisFrame, image = photo, bg = bg)
        self.MetNadpis2.grid(row = 0, column = 0, sticky = "e", columnspan = 50)
        self.MetNadpis2.image = photo
        
        
        for x in range(0,5):
            self.CHNazev[x] = tkinter.Entry(self.MetADCFrame, width = 10)
            self.CHCheck[x] = tkinter.BooleanVar()
            self.CHCheckbox[x] = tkinter.Checkbutton(self.MetADCFrame, variable = self.CHCheck[x], bg = bg, bd = 0)
            self.CHCheckbox[x].grid(row = x+1, column = 2)
            self.CHNazev[x].grid(row = x+1, column = 3)
            self.CH[x] = tkinter.Label(self.MetADCFrame, text=("CH" + str((x+1))), bg = bg,font=("Serif", velikostF))
            self.CH[x].grid(row = x+1, column = 1, sticky = "e")
            self.vyplnovaciLabel = tkinter.Label(self.MetADCFrame, text=("                                       "), bg = bg)
            
            self.CHFilterV[x] = tkinter.StringVar()
            self.CHFilter[x] = ttk.Combobox(self.MetADCFrame, width = 10, textvariable = self.CHFilterV[x])
            self.CHFilter[x]["values"] = ("SINC1", "SINC2", "SINC3", "SINC4", "FIR")
            self.CHFilter[x].grid(row = x+1, column = 4, sticky = "w", columnspan = 2)
            self.CHFilter[x].current()
            
            self.CHGainV[x] = tkinter.StringVar()
            self.CHGain[x] = ttk.Combobox(self.MetADCFrame, width = 10, textvariable = self.CHGainV[x])
            self.CHGain[x]["values"] = ("1", "2", "4", "8","16","32")
            self.CHGain[x].grid(row = x+1, column = 6, sticky = "w", columnspan = 2)
            self.CHGain[x].current()
            
            self.CHRateV[x] = tkinter.StringVar()
            self.CHRate[x] = ttk.Combobox(self.MetADCFrame, width = 10, textvariable = self.CHRateV[x])
            self.CHRate[x]["values"] = ("2.5", "5", "10", "16.66", "20", "50", "60", "100", "400", "1200", "2400", "4800", "7200", "14400", "19200", "38400")
            self.CHRate[x].grid(row = x+1, column = 8, sticky = "w", columnspan = 2)
            self.CHRate[x].current()
        
        self.CHLabel[0] = tkinter.Label(self.MetADCFrame, text = "Název", bg = bg, font=("Serif", 15))
        self.CHLabel[1] = tkinter.Label(self.MetADCFrame, text = "Filtr", bg = bg, font=("Serif", 15))
        self.CHLabel[2] = tkinter.Label(self.MetADCFrame, text = "Gain", bg = bg, font=("Serif", 15))
        self.CHLabel[3] = tkinter.Label(self.MetADCFrame, text = "Rate", bg = bg, font=("Serif", 15))
        
        self.CHLabel[0].grid(row = 0, column = 3)
        self.CHLabel[1].grid(row = 0, column = 4)
        self.CHLabel[2].grid(row = 0, column = 6)
        self.CHLabel[3].grid(row = 0, column = 8)
        
        self.const = int((root.winfo_screenwidth()))
        
        self.img = Image.open("tabulkaMereni.png")
        self.width2, self.height2 = self.img.size
        self.img = self.img.resize((self.const, int((self.const / self.width2) * self.height2)), Image.ANTIALIAS)
        photo = ImageTk.PhotoImage(self.img)
            
        self.MetNadpis3 = tkinter.Label(self.MetTabulkaNadpisFrame, image = photo, bg = bg)
        self.MetNadpis3.grid(row = 0, column = 0, sticky = "e", columnspan = 50)
        self.MetNadpis3.image = photo
        
        sf = ScrolledFrame(self.MetTabulkaFrame, width=640, height=300, bg=bg)
        sf.pack(side = "bottom",expand=1, fill="both") 

        # Bind the arrow keys and scroll wheel
        sf.bind_arrow_keys(self.MetTabulkaFrame)
        sf.bind_scroll_wheel(self.MetTabulkaFrame)

        frame = sf.display_widget(tkinter.Frame, bg =bg)
        
        self.MetTab = [0] * 420
        
        for i in range(20):
            for l in range(1,21):
                self.MetTab[l+(i*10)] = tkinter.Label(frame, height = 2, width = 5, bg = bg,text = "ahoj", font=("Serif", 15), borderwidth=2, relief="solid", bd = 2)
                self.MetTab[l+(i*10)].grid(row = i, column = l)
        
        self.readINI()
        self.setMetod(self.config["METODIKA"]["file"])
        
        self.updateScrollRegion()
 
    def calculateData(self):        
        self.limMinArr = [float("inf")] * 100
        self.limMaxArr = [-float("inf")] * 100

        self.limMinArr2 = [float("inf")] * 100
        self.limMaxArr2 = [-float("inf")] * 100
        
        self.ylimX = -float("inf")
        self.ylimN = float("inf")
        
        if(self.cycle == 0):
            for l in range(1, self.axes):
                if(self.extension == 'dat'):
                    self.graph_y = self.grafY[l-1]
    
                else:
                    self.graph_y = [row[l] for row in self.data_arr]
                    self.graph_y = [float(item) for item in self.graph_y]
                    
                    for k in range(len(self.graph_y)):
                        if(self.graph_y[k] == float("inf") or self.graph_y[k] == -float("inf")):
                            self.graph_y[k] = self.graph_y[k - 1]
                    
                self.grafY[l-1] = self.graph_y
                    
                self.y_max[l-1] = max(self.graph_y)
                self.y_min[l-1] = min(self.graph_y)
               
        self.proof = 1
        self.ylimX = np.max(self.y_max)
        self.ylimN = np.min(self.y_min) 
          
        self.ylimX2 = np.max(self.y_max)
        self.ylimN2 = np.min(self.y_min)
            
        self.xminEntry = min(self.graph_x)
        self.xmaxEntry = max(self.graph_x)

        if(self.proof == 1):                
            self.ax1.set_ylim(self.ylimN, self.ylimX)
            self.ax1.set_xlim(min(self.graph_x), max(self.graph_x))
            self.chartSpace()
    
    def destroyWidgets(self):    
        self.ax1.clear()
        
        self.histogramFrame.destroy()
        self.histogramFrame = Frame(self.frame, width = 300, height = self.myheight*1.5)
        self.histogramFrame.pack()
        
        self.cycle = 0
        self.canvas.draw()
    
    
    
    def csvUpload(self):
        self.FIPS = False
        self.file_path = filedialog.askopenfilename()
        
        
        self.loading = tkinter.Label(root, text="Loading ...     ", font = "Helevtica 20 bold")
        self.loading.place(x = 0, y = int(self.myheight - 130))
        self.canvas.draw()
        
        self.filename, self.extension = self.file_path.split(".")
        
        if(self.cycle == 1):
            self.destroyWidgets()
            self.resetVar()
            self.cycle = 0
        
        
        
        root.wm_title("CromaDLo - " + self.file_path)
        
        self.startbutton.destroy()
        self.canvas.draw()
        self.animation=False
        self.updateChart()
        
    def saveMetod(self):
         config = []
         file = filedialog.asksaveasfilename(parent=root,title='Save')
         f = open(file + '.csv', "w")
         
         self.readINI()
         self.config["METODIKA"]["file"] = (file + '.csv')
         self.writeINI()
         
         
         for i in range(5):
             config.append([self.CHCheck[i].get(),";", self.CHNazev[i].get(),";", self.CHFilter[i].get(),";",self.CHGain[i].get(),";", self.CHRate[i].get(),])
             
         config.append([";",self.MetNazevMetodikaEntry.get(),";",self.MetNazevLaboratorEntry.get(),";",self.MetJmenoLaborantEntry.get(),self.MetPopisEntry.get("1.0", END),self.MetDelkaEntry.get(),";", self.MetToleranceEntry.get(),";", self.MetNastrikEntry.get(), "\n"])

         for radky in range(len(config)):
             for sloupce in range(len(config[radky])):
                 f.write(str(config[radky][sloupce]))
                 
         f.close()
         
         self.saveMetodINI()
    
    def saveMetodINI(self):
        self.readINI()
        self.config["METODIKA"]["delka"] = self.MetDelkaEntry.get()
        self.config["METODIKA"]["tolerance"] = self.MetToleranceEntry.get()
        self.config["METODIKA"]["objem"] = self.MetNastrikEntry.get()
        
        self.config["ADC"]["enableCH1"] = str(self.CHCheck[0].get())
        self.config["ADC"]["enableCH2"] = str(self.CHCheck[1].get())
        self.config["ADC"]["enableCH3"] = str(self.CHCheck[2].get())
        self.config["ADC"]["enableCH4"] = str(self.CHCheck[3].get())
        self.config["ADC"]["enableCH5"] = str(self.CHCheck[4].get())
        
        self.config["ADC"]["gainCH1"] = str(self.CHGain[0].get())
        self.config["ADC"]["gainCH2"] = str(self.CHGain[1].get())
        self.config["ADC"]["gainCH3"] = str(self.CHGain[2].get())
        self.config["ADC"]["gainCH4"] = str(self.CHGain[3].get())
        self.config["ADC"]["gainCH5"] = str(self.CHGain[4].get())
        
        self.config["ADC"]["filterCH1"] = str(self.CHFilter[0].get())
        self.config["ADC"]["filterCH2"] = str(self.CHFilter[1].get())
        self.config["ADC"]["filterCH3"] = str(self.CHFilter[2].get())
        self.config["ADC"]["filterCH4"] = str(self.CHFilter[3].get())
        self.config["ADC"]["filterCH5"] = str(self.CHFilter[4].get())
        
        self.config["ADC"]["rateCH1"] = str(self.CHRate[0].get())
        self.config["ADC"]["rateCH2"] = str(self.CHRate[1].get())
        self.config["ADC"]["rateCH3"] = str(self.CHRate[2].get())
        self.config["ADC"]["rateCH4"] = str(self.CHRate[3].get())
        self.config["ADC"]["rateCH5"] = str(self.CHRate[4].get())
        
        
        self.writeINI()
         
    def readcsv(self, filename):
        ifile = open(filename, "r")
        
        reader = csv.reader(ifile,delimiter=";")
        a = []

        for i, row in enumerate(reader):        
            if(i >= self.rownum):
                a.append(row)
                
                self.rownum +=1
                
        ifile.close()
        
        if(self.cycle == 1 and self.animation == True):
            if(len(a) != 0):
                if(len(a[0]) == self.axes):
                        
                    for l in range(0, self.axes - 1):
                        for n in range(len(a)):
                            self.grafX[l] = np.append(self.grafX[l], float(a[n][0]))                               
                            self.grafY[l] = np.append(self.grafY[l], float(a[n][l + 1]))
                            
                        self.grafX[l] = self.grafX[l].tolist()
                        self.grafY[l] = self.grafY[l].tolist()
        
        return a
    
    def liveChart(self):
        if(self.animation == True):
            if(self.cycle == 1):
                self.readcsv(self.csvfile)
           
            self.updateChart()
        
            self.zoomXmin = np.amin(self.grafX)
            self.zoomXmax = np.amax(self.grafX)
            self.zoomYmax = np.amax(self.grafY)

            self.ax1.set_xlim(self.zoomXmin, self.zoomXmax)
            
            self.canvas.draw()
            root.after(500, self.liveChart)
        
    def updateChart(self):
        self.ax1.clear()
        
        self.ax1.grid(color="#e1e8e3", linestyle='-', linewidth=1)
                      
        if(self.cycle == 0):
            self.data_arr = self.readcsv(self.file_path)
            print(self.data_arr)
            del(self.data_arr[0])
            
            
            
            
            
            self.graph_x = [row[0] for row in self.data_arr]
            self.graph_x = [float(item) for item in self.graph_x]
            
            self.axes = len(self.data_arr[1])
           
            
            self.colormap = plt.cm.gist_ncar  
            self.colors = [self.colormap(i) for i in np.linspace(0, 1, self.axes)]
            
            self.grafX = [0] * (self.axes-1)
            self.grafY = [0] * (self.axes-1)
            
            
            self.colormap = plt.cm.gist_ncar
            self.colors = [self.colormap(i) for i in np.linspace(0, 1, self.axes)]
            
            for i in range(self.axes):
                self.col[i] = (int(self.colors[i][0] * 255), int(self.colors[i][1] * 255), int(self.colors[i][2] * 255))
            
            
            
            
            for l in range(1, self.axes):
                self.graph_y = [row[l] for row in self.data_arr]
                self.graph_y = [float(item) for item in self.graph_y]
                
                for k in range(len(self.graph_y)):
                    if(self.graph_y[k] == float("inf") or self.graph_y[k] == -float("inf")):
                        self.graph_y[k] = self.graph_y[k - 1]
                        
                self.grafX[l - 1] = self.graph_x
                
                self.ax1.plot(self.grafX[l - 1], self.graph_y, self.style[l-1])
            
            self.ax1pos = [0] * len(self.ax1.lines)
            self.calculateData()
            
            
            self.position = 0
            
            
            
            for l in range(0, self.axes-1):
                self.ax1pos[self.position] = l
                self.position = self.position + 1
            

            
            
            for i,j in enumerate(self.ax1.lines):
                j.set_color(self.colors[i])
                
            if(self.animation == False):
                self.loading.destroy()
            
            self.drawChartWidgets()
            self.canvas.draw()
        
        else:
            self.position = 0
            
            for l in range(0, self.axes - 1):
                
                self.ax1.plot(self.grafX[l], self.grafY[l], self.style[l], color = self.colors[l])
                
                if(self.globalstop == 1):
                    self.canvas.draw()
                    self.adjustView()
        
        
       
        
        self.cycle = 1
        self.setVis()
        self.checkVis = [False] * self.axes
        
        if(self.animation==True):
            for i in range(self.axes - 1):
                self.labelAnalyze[i].configure(text=f'{self.grafY[i][-1]:.3f}')
        
    def chartSpace(self):
        lim = self.ax1.get_ylim()
        self.ax1.set_ylim(lim[0] - ((lim[1] - lim[0]) / 100), lim[1] + ((lim[1] - lim[0]) / 100) * 3)
        
        
        
app = Chart()
app.init()

root.mainloop()
