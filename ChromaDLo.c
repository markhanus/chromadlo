#include "ADS1263.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
#include <errno.h>
#include <wiringPiSPI.h>
#include <wiringPi.h>
#include <unistd.h>
#include <stdint.h>
#include "libconfigini/src/configini.h"

#define CONFIGREADFILE		"status.ini"
#define VREF 2.5    //2.5 V
#define RESOLUTION 2147483648 // 2^(32-1)

double channel[5], elapsed_seconds;
int AD_DRDY = 27;
int AD_START = 22;
int RELE1 = 18;
int RELE2 = 23;
int RELE3 = 24;
int RELE4 = 25;

int cycle, check = 0, sample_rate = 50, channelStatus[5], actualchannel = 0;
int statusFlag = 0, previewFlag = 0;

unsigned char Status[15], DB1[15], DB2[15], DB3[15], DB4[15], CRCcode[15], Regis-ter_data[20];
unsigned char Rate=0, Filter=0, Gain_PEL=0, Gain_NTC=0, Current=0;
char timeINI[1024];

char enableCh[5][1024];
char gainCh[5][1024];
char filterCh[5][1024];
char rateCh[5][1024];


int Set_VBIAS(unsigned char VBIAS)
{
    char buffer[] = {0x41, 0x00, VBIAS};
    
    wiringPiSPIDataRW(0, buffer, 3); return 0;
}

int Set_Chop(unsigned char Chop)
{
    char buffer[] = {0x43,0x00, Chop};
    wiringPiSPIDataRW(0, buffer, 3); return 0;
}

int Set_Filter(unsigned char Filter)
{
    char buffer[] = {0x44, 0x00, Filter << 5};
    wiringPiSPIDataRW(0, buffer, 3); return 0;
}

int Set_PGA_Rate(unsigned char PGA, int Rate)
{
   unsigned char charRate;
   if(Rate <= 2) charRate = Rate_2;
   else if(Rate <= 5) charRate = Rate_5;
   else if(Rate <= 10) charRate = Rate_10;
   else if(Rate <= 16) charRate = Rate_16;
   else if(Rate <= 20) charRate = Rate_20;
   else if(Rate <= 50) charRate = Rate_50;
   else if(Rate <= 60) charRate = Rate_60;
   else if(Rate <= 100) charRate = Rate_100;
   else if(Rate <= 400) charRate = Rate_400;
   else if(Rate <= 1200) charRate = Rate_1200;
   else if(Rate <= 2400) charRate = Rate_2400;
   else if(Rate <= 4800) charRate = Rate_4800;
   else if(Rate <= 7200) charRate = Rate_7200;
   else if(Rate <= 14400) charRate = Rate_14400;
   else if(Rate <= 19200) charRate = Rate_19200;
   else charRate = Rate_38400;
   
   char buffer[] = {0x45, 0x00, (PGA << 4) | charRate};
   wiringPiSPIDataRW(0, buffer, 3); return 0;   
}

int Set_Channel(unsigned char posChannel, unsigned char negChannel)
{
   char buffer[] = {0x46, 0x00, (posChannel << 4) | negChannel};
   wiringPiSPIDataRW(0, buffer, 3); return 0;
   
}

int ADC_Read(int ChannelNo)
{    
    char buffer[6];
    for(int i = 0; i < 6; i++)
    {
        buffer[i] = 0x00;
    }
     
    wiringPiSPIDataRW(0, buffer, 6);
    Status[ChannelNo] = buffer[0];
    DB1[ChannelNo] = buffer[1];
    DB2[ChannelNo] = buffer[2];
    DB3[ChannelNo] = buffer[3];
    DB4[ChannelNo] = buffer[4];
    CRCcode[ChannelNo] = buffer[5];
    
    return 0;
}

void ReadRegisters()
{
        char buffer[9] = {0x20, 0x06};
        
        
        for(int i = 2; i < 9; i++)
        {
            buffer[i] = 0x00;
        }
        wiringPiSPIDataRW(0, buffer, 9);
        for (int i=0; i<9; i++)
        {
            printf("Register %d: %2X  \n\r", i, buffer[i]);   
        }
}
// ************************************************************************************

void Diagnostika()
{    
	digitalWrite(AD_START,0); delay(1);
    char buffer[0] = {0x06};
    wiringPiSPIDataRW(0, buffer, 1);
    delay(1);
    
    // AVDD
    Set_Chop(Chop_OFF); Set_Filter(SINC1);
    Set_PGA_Rate(PGA_1,50); Set_Channel(Kod_AVDD, Kod_AVDD);
    delay(10); digitalWrite(AD_START,1);
    while(digitalRead(AD_DRDY) == 1);
    ADC_Read(Kod_AVDD); digitalWrite(AD_START,0);
    
	// DVDD
    Set_Chop(Chop_OFF); Set_Filter(SINC1);
    Set_PGA_Rate(PGA_1,50); Set_Channel(Kod_DVDD, Kod_DVDD);
    delay(10); digitalWrite(AD_START,1);
    while(digitalRead(AD_DRDY) == 1);
    ADC_Read(Kod_DVDD); digitalWrite(AD_START,0);

	//  Temp_Chip
    Set_Chop(Chop_OFF); Set_Filter(SINC1);
	Set_PGA_Rate(PGA_1, 50); Set_Channel(Kod_Temp, Kod_Temp);
    delay(10); digitalWrite(AD_START,1);
    while(digitalRead(AD_DRDY) == 1);
    ADC_Read(Kod_Temp); digitalWrite(AD_START,0);
    
    unsigned long AVDD, DVDD, Temp;
    
    AVDD =  DB1[Kod_AVDD] << 24 | DB2[Kod_AVDD] << 16 | DB3[Kod_AVDD] << 8 | DB4[Kod_AVDD];
    AVDD = AVDD / 214748.3648;
    DVDD =  DB1[Kod_DVDD] << 24 | DB2[Kod_DVDD] << 16 | DB3[Kod_DVDD] << 8 | DB4[Kod_DVDD];
    DVDD = DVDD / 214748.3648;
    Temp =  DB1[Kod_Temp] << 24 | DB2[Kod_Temp] << 16 | DB3[Kod_Temp] << 8 | DB4[Kod_Temp];
    Temp = Temp / 4768.24415;

    printf("AVDD: %d %d %d %d %d %d \n\r", Status[Kod_AVDD], DB1[Kod_AVDD], DB2[Kod_AVDD], DB3[Kod_AVDD], DB4[Kod_AVDD], CRCcode[Kod_AVDD]);
    printf("DVDD: %d %d %d %d %d %d\n\r", Status[Kod_DVDD], DB1[Kod_DVDD], DB2[Kod_DVDD], DB3[Kod_DVDD], DB4[Kod_DVDD], CRCcode[Kod_DVDD]);
    printf("Temp: %d %d %d %d %d %d\n\r", Status[Kod_Temp], DB1[Kod_Temp], DB2[Kod_Temp], DB3[Kod_Temp], DB4[Kod_Temp], CRCcode[Kod_Temp]);
    printf("AVDD: %i     DVDD: %i    Temp: %i\n\r", AVDD, DVDD, Temp);
}

void Mereni()
{
    ADC_Read(actualchannel);
    channel[actualchannel] = (DB1[actualchannel] << 24 | DB2[actualchannel] << 16 | DB3[actualchannel] << 8 | DB4[actualchannel]) * (VREF / RESOLUTION) * 1000;
            
    if(channel[actualchannel] >= 0x80000000) 
    {
            channel[actualchannel] = channel[actualchannel] - 0x80000000; 
    }

    check = 1;
    cycle++;
}

int setADC(int i)
{
    if(strcmp(enableCh[i], "True") == 0)
    {
        if(strcmp(filterCh[i], "SINC1") == 0) 
        {Set_Filter(SINC1);}
        else if(strcmp(filterCh[i], "SINC2") == 0) 
        {Set_Filter(SINC2);}
        else if(strcmp(filterCh[i], "SINC3") == 0) 
        {Set_Filter(SINC3);}
        else if(strcmp(filterCh[i], "SINC4") == 0) 
        {Set_Filter(SINC4);}
        else if(strcmp(filterCh[i], "FIR") == 0) 
        {Set_Filter(FIR);}
                
        if(strcmp(gainCh[i], "1") == 0)
        {Set_PGA_Rate(PGA_1, (atoi(rateCh[i])));}
        else if(strcmp(gainCh[i], "2") == 0)
        {Set_PGA_Rate(PGA_2, (atoi(rateCh[i])));}
        else if(strcmp(gainCh[i], "4") == 0)
        {Set_PGA_Rate(PGA_4, (atoi(rateCh[i])));}
        else if(strcmp(gainCh[i], "8") == 0)
        {Set_PGA_Rate(PGA_8, (atoi(rateCh[i])));}
        else if(strcmp(gainCh[i], "16") == 0)
        {Set_PGA_Rate(PGA_16, (atoi(rateCh[i])));}
        else if(strcmp(gainCh[i], "32") == 0)
        {Set_PGA_Rate(PGA_32, (atoi(rateCh[i])));}
        Set_Channel(i+i,i+i+1);
    }
}

char* readADCINI()
{
    Config *cfg = NULL;
    if (ConfigReadFile(CONFIGREADFILE, &cfg) != CONFIG_OK) {
		printf("failt to open");
	}
	char enable[20], filter[20], gain[20], rate[20];

    for(int i = 0; i < 5; i++)
    {
		sprintf(enable,"enablech%i",i+1);
		ConfigReadString(cfg, "ADC", enable, enableCh[i], sizeof(enableCh[i]));
        if(strcmp(enableCh[i], "True") == 0)
        {
            sprintf(filter,"filterch%i", i+1);
            sprintf(gain,"gainch%i", i+1);
            sprintf(rate,"ratech%i", i+1);
            
            ConfigReadString(cfg, "ADC", filter, filterCh[i], sizeof(filterCh[i]));
            ConfigReadString(cfg, "ADC", gain, gainCh[i], sizeof(gainCh[i]));
            ConfigReadString(cfg, "ADC", rate, rateCh[i], sizeof(rateCh[i]));
        }
    }
}

char* readINI()
{
	Config *cfg = NULL;
	if (ConfigReadFile(CONFIGREADFILE, &cfg) != CONFIG_OK) {
	printf("failt to open");
	}
    
	char s[1024];
	char b[1024];
    char previewINI[1024];

	ConfigReadString(cfg, "STATUS", "state", s, sizeof(s));
    ConfigReadString(cfg, "STATUS","preview", previewINI, sizeof(previewINI));
    ConfigReadString(cfg, "METODIKA", "time", timeINI, sizeof(timeINI));
    
    if(strcmp(s, "start") == 0)
	{
        statusFlag = 1;
    }
    else if(strcmp(s, "stop") == 0)
	{
	  statusFlag = 0; 
	}
    
    if(strcmp(previewINI, "Enable") == 0)
    {
        previewFlag = 1;
    }
	else if(strcmp(previewINI, "Disable") == 0)
    {
        previewFlag = 0;
    }
	ConfigFree(cfg);
}

int main()
{
    int firstcycle = 0;
    char filename[1024];
    
    wiringPiSetup();
    wiringPiSetupGpio();
    wiringPiSPISetupMode(0, 1000000, 1);
    
    pinMode(AD_DRDY, INPUT);
    pinMode(AD_START, OUTPUT);
    pinMode(RELE1, OUTPUT);
    pinMode(RELE2, OUTPUT);
    pinMode(RELE3, OUTPUT);
    pinMode(RELE4, OUTPUT);
    
    digitalWrite(RELE1,0);
    digitalWrite(RELE2,0);
    digitalWrite(RELE3,0);
    digitalWrite(RELE4,0);
    
    Diagnostika();
    
    char buffer[1] = {0x06};
    wiringPiSPIDataRW(0, buffer, 1);
    readADCINI();
    ReadRegisters();
    
    digitalWrite(AD_START,0); delay(1);
    wiringPiSPIDataRW(0, buffer, 1); // Reset ADC1262
    delay(1);
    
    Set_Chop(Chop_OFF); 
    Set_Filter(SINC1);
    Set_PGA_Rate(PGA_1,sample_rate); 
    Set_Channel(8, 9); 
      
    while(statusFlag == 0 && previewFlag == 0) {readINI(); readADCINI();}
    sprintf(filename,"/home/pi/Documents/ChromaDLo_program/measurementFiles/%s.csv",timeINI);
    wiringPiISR(AD_DRDY, INT_EDGE_FALLING, Mereni);

    if(previewFlag == 1)
    {
        FILE *k = fopen("ChromaDLo_data.csv", "w+");
        fprintf(k,"");
        fclose(k);
        digitalWrite(AD_START,1);
        
        clock_t start = clock();
        while(previewFlag == 1)
        {
            readINI();
            if(check == 1)
            {
                for(int i = 0; i < 5;i++)
                {
                    check = 0;
                    
                    actualchannel = i;
                    setADC(i);
                    Set_Channel(i+i, i+i+1);
                    while(check == 0);
                    printf("%i", check);
                }

                clock_t end = clock();
                elapsed_seconds = ((double)(end-start))/ CLOCKS_PER_SEC;
                
                FILE *f = fopen("ChromaDLo_data.csv", "a+");
                fprintf(f, "%.3f;%.3f;%.3f;%.3f;%.3f;%.3f\n", elapsed_seconds, chan-nel[0], channel[1], channel[2], channel[3], channel[4]);
                fflush(f);
                fclose(f);
                printf(" %.3f;%.3f;%.3f;%.3f;%.3f;%.3f\n", elapsed_seconds, chan-nel[0], channel[1], channel[2], channel[3], channel[4]);
                check = 0;
            }
        }
        
        
    }

    readINI();
    
    sprintf(filename,"/home/pi/Documents/ChromaDLo_program/measurementFiles/%s.csv",timeINI);
    
    FILE *l = fopen("ChromaDLo_data.csv", "w+");
    fprintf(l,"");
    fclose(l);
    
    FILE *g = fopen(filename, "w+");
    fprintf(g,"");
    fclose(g);
    
    digitalWrite(RELE1,1);
    digitalWrite(AD_START,1);
    
    clock_t start = clock();
    while(statusFlag == 1)
    {

        readINI();
        if(check == 1)
        {
            if(firstcycle == 0)
            {
                digitalWrite(RELE1,0);
                firstcycle = 1;
            }
            
            for(int i = 0; i < 5;i++)
            {
                check = 0;
				actualchannel = i;
                setADC(i);
                Set_Channel(i+i, i+i+1);

                while(check == 0);
            }
            
            clock_t end = clock();
            elapsed_seconds = ((double)(end-start))/CLOCKS_PER_SEC;
            
            FILE *fp = fopen("ChromaDLo_data.csv", "a+");
            fprintf(fp, "%.3f;%.3f;%.3f;%.3f;%.3f;%.3f\n", elapsed_seconds, chan-nel[0], channel[1], channel[2], channel[3], channel[4]);
            fflush(fp);
            fclose(fp);
            
            FILE *p = fopen(filename, "a+");
            fprintf(p, "%.3f;%.3f;%.3f;%.3f;%.3f;%.3f\n", elapsed_seconds, channel[0], channel[1], channel[2], channel[3], channel[4]);
            fflush(p);
            fclose(p);
            
            printf("%.3f;%.3f;%.3f;%.3f;%.3f;%.3f\n", elapsed_seconds, channel[0], channel[1], channel[2], channel[3], channel[4]);
			check = 0;
        }
    }
    digitalWrite(RELE2,1); delay(100); digitalWrite(RELE2,0);
}
